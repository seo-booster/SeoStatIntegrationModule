const moment = require('moment');
const Client = require('../lib/Client/HttpClient');

mockedRequest = {
    get: () => {}
};

describe('[HttpClient]', () => {
    let client;
    beforeEach(() => {
        client = new Client(mockedRequest);
    });

    it('should create instance of HttpClient', () => {
        expect(client).toBeDefined();
    });

    it('should call request library with correct context', () => {
        const payload = {
            uri: 'localhost',
            code: '123',
            start_date: '2019-01-01'
        };
        const spy = spyOn(mockedRequest, 'get');
        client.getStat(payload);
        expect(spy).toHaveBeenCalledWith(payload.uri, {
            qs: {
                code: payload.code,
                start_date: '2019-01-01',
            },
            headers: {
                Accept: 'application/json'
            },
            json: true
        });
    });

    it('should call request library with defaults passed to context', () => {
        const payload = {
            uri: 'localhost',
            code: '123',
        };
        const spy = spyOn(mockedRequest, 'get');
        client.getStat(payload);
        expect(spy).toHaveBeenCalledWith(payload.uri, {
            qs: {
                code: payload.code,
                start_date: moment().startOf('month').format('YYYY-MM-DD')
            },
            headers: {
                Accept: 'application/json'
            },
            json: true
        });
    });
});