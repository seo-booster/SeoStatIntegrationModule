const SSIM = require('../lib/Api/SSIM');

const client = {
    getStat: async () => 'test',
};
const manager = {
    setConfiguration: () => {},
    getConfiguration: () => {},
};
const repository = {
    save: async () => {},
};

describe('[SSIM]', () => {
    let ssim;
    beforeEach(() => {
        ssim = new SSIM({ client, manager });
    });

    it('should create instance of SSIM', () => {
        expect(ssim).toBeDefined();
    });

    it('should call manager.setConfiguration for every passed configuration', () => {
        const spy = spyOn(manager, 'setConfiguration');
        const configurations = [
            { name: 'test' },
            { name: 'test2' },
        ];
        ssim.configure(configurations);
        expect(spy).toHaveBeenCalledTimes(2);
    });

    it('should call manager.getConfiguration with passed configuration name on getStat', () => {
        const spy = spyOn(manager, 'getConfiguration');
        ssim.getStat('test');
        expect(spy).toHaveBeenCalledWith('test');
    });

    it('should call client.getStat with named configuration on getStat', () => {
        const spy = spyOn(client, 'getStat');
        const configuration = { uri: 'localhost' };
        spyOn(manager, 'getConfiguration').and.returnValue(configuration);
        ssim.getStat('test');
        expect(spy).toHaveBeenCalledWith(configuration);
    });

    it('should pass additional context to client.getStat on getStat', () => {
        const spy = spyOn(client, 'getStat');
        const configuration = { uri: 'localhost' };
        const ctx = { start_date: '2019-01-02' };
        spyOn(manager, 'getConfiguration').and.returnValue(configuration);
        ssim.getStat('test', ctx);
        expect(spy).toHaveBeenCalledWith({ ...configuration, ...ctx });
    });

    it('should call repository.save if one is configured with response as payload on getStat', (done) => {
        const spy = spyOn(repository, 'save');
        ssim.setRepository(repository);
        ssim.getStat('test').then(() => {
            expect(spy).toHaveBeenCalledWith('test');
            done();
        });
    });
});