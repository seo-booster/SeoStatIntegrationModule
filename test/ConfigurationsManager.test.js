const ConfigurationsManager = require('../lib/ConfigurationsManager/ConfigurationsManager');

class TestConfiguration {}

describe('[ConfigurationsManager]', () => {
    let cm;
    beforeEach(() => {
        cm = new ConfigurationsManager(TestConfiguration);
    });

    it('should create instance of ConfigurationsManager', () => {
        expect(cm).toBeDefined();
    });

    it('should create named configuration on setConfiguration', () => {
        cm.setConfiguration({ name: 'test' });
        const config = cm.getConfiguration('test');
        expect(config).toBeInstanceOf(TestConfiguration);
    });

    it('should throw CONF_NAME_EXPECTED if name was ommited on setConfiguration', () => {
        expect(() => {
            cm.setConfiguration({});
        }).toThrow('CONF_NAME_EXPECTED');
    });

    it('should throw CONF_NAME_EXPECTED if name was ommited on setConfiguration', () => {
        expect(() => {
            cm.getConfiguration('test');
        }).toThrow('CONF_NOT_FOUND');
    });

    it('should return true if configuration exists on isConfigurationExists', () => {
        expect(ConfigurationsManager.isConfigurationExists('test', { test: {} })).toEqual(true);
    });

    it('should return false if configuration does not exists on isConfigurationExists', () => {
        expect(ConfigurationsManager.isConfigurationExists('test', {})).toEqual(false);
    });

});