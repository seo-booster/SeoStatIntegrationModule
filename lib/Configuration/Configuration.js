class Configuration {
    constructor({ uri, code, start_date, headers }) {
        this.uri = uri;
        this.code = code;
        this.start_date = start_date;
        this.headers = headers;
    }
}

module.exports = Configuration;