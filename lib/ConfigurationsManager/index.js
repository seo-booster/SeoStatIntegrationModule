const Configuration = require('../Configuration');
const ConfigurationManager = require('./ConfigurationsManager');

module.exports = new ConfigurationManager(Configuration);