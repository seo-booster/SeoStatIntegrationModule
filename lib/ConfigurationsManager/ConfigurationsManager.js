class ConfigurationsManager {
    constructor(Configuration) {
        this.Configuration = Configuration;
        this.configurations = {};
    }

    static isConfigurationExists(name, configurations) {
        return Object.prototype.hasOwnProperty.call(configurations, name);
    }

    setConfiguration({ name, ...options }) {
        if (name) {
            this.configurations[name] = new this.Configuration(options);
        } else {
            throw new Error('CONF_NAME_EXPECTED');
        }
    }

    getConfiguration(name) {
        if (ConfigurationsManager.isConfigurationExists(name, this.configurations)) {
            return this.configurations[name];
        } else {
            throw new Error('CONF_NOT_FOUND');
        }
    }
}

module.exports = ConfigurationsManager;