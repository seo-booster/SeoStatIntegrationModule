const moment = require('moment');

class HttpClient {
    constructor(http) {
        this.http = http;
        this.headers = {
            Accept: 'application/json',
        };
    }

    static getStartDate() {
        return moment().startOf('month').format('YYYY-MM-DD');
    }

    getStat({ uri, code, start_date, headers = {} }) {
        return this.http.get(uri, {
            qs: {
                code,
                start_date: start_date ? start_date : HttpClient.getStartDate()
            },
            headers: { ...this.headers, ...headers },
            json: true
        });
    }
}

module.exports = HttpClient;