const request = require('request-promise-native');
const Client = require('./HttpClient');

module.exports = new Client(request);