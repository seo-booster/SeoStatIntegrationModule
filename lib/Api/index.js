const client = require('../Client');
const manager = require('../ConfigurationsManager');
const SSIM = require('./SSIM');

module.exports = new SSIM({client, manager});