class SSIM {
    constructor({ client, manager }) {
        this.client = client;
        this.manager = manager;
    }

    setRepository(repository) {
        this.repository = repository;
        return this;
    }

    configure(configurations) {
        configurations.forEach(configuration => {
            this.manager.setConfiguration(configuration);
        });
        return this;
    }

    async getStat(configurationName, context = {}) {
        const configuration = this.manager.getConfiguration(configurationName);
        const response = await this.client.getStat({...configuration, ...context});
        if (this.repository) {
            await this.repository.save(response);
        }
        return response;
    }
}

module.exports = SSIM;